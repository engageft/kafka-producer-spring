package com.engageft.consumer.controller;

import com.engageft.consumer.KafkaProducerApplication;
import com.engageft.consumer.messages.MessageProducer;
import com.engageft.consumer.model.Auth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthTransactionController {

    private final Logger logger = LoggerFactory.getLogger(KafkaProducerApplication.class);

    private MessageProducer messageProducer;

    @Autowired
    public AuthTransactionController(MessageProducer messageProducer) {
        this.messageProducer = messageProducer;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void postAuth(@RequestBody Auth auth) {
        logger.info("received Auth " + auth);
        messageProducer.sendAuthMessage(auth);
    }

}
