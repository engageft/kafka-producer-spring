package com.engageft.consumer.messages;

import com.engageft.consumer.KafkaProducerApplication;
import com.engageft.consumer.model.Auth;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class MessageGenerator {

    private final Logger logger = LoggerFactory.getLogger(KafkaProducerApplication.class);

    private MessageProducer producer;

    @Value(value = "${auth.topic.name}")
    private String authTopicName;

    @Autowired
    public MessageGenerator(MessageProducer producer) {
        this.producer = producer;
    }

    @PostConstruct
    public void generateMessages()  {

        producer.sendMessage("Hello, World!");

        for (int i = 0; i < 50; i++) {
            int partition = RandomUtils.nextInt(0, 1);
            logger.info("sending message number " + i + " to " + partition);
            producer.sendMessageToPartition("Hello To Partitioned Topic!", partition);
        }

        producer.sendMessageToFiltered("Hello Baeldung!");
        producer.sendMessageToFiltered("Hello World!");

        producer.sendAuthMessage(getAuth());
    }

    Auth getAuth() {
        final Auth auth = new Auth();
        auth.setAcquiringBankCode(Integer.toString(RandomUtils.nextInt(1000, 10000)));
        auth.setTenant("1234567890");
        return auth;
    }
}
